<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Diagnostico extends Model
{
    protected $table='diagnostico';
    protected $primary_key='id';

    protected $fillable=[
        'id',
        'descripcion',
        'created_at',
        'updated_at',
    ];
}
