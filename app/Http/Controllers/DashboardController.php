<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\InfoProfesional;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public function index()
    {
        return view('layouts.dashboard');
    }
    public function getProfile($id)
    {
        switch (auth()->user()->tipo) {
            case ("Profesional"):
                $user = User::find($id);

                return view('dashboard.profile', compact('user'));
                break;
            case ("Paciente"):
                return view('dashboard.profile');
                break;
        }
    }
    public function back()
    {
        return back();
    }
    public function update(Request $request)
    {
        user::where('id', auth()->user()->id)
            ->update([
                'name' => $request->input('name'),
                'apellido' => $request->input('apellido'),
                'direccion' => $request->input('direccion'),
                'comuna' => $request->input('comuna'),
                'telefono' => "569" . $request->input('telefono'),
                'nickname' => $request->input('nickname'),
                'email' => $request->input('correo'),
                //'region' => $request->input('region')
            ]);
        return back();
    }
    public function postProfileImage(Request $request)
    {
        $user = auth::user();
        $extension = $request->file('file')->getClientOriginalExtension();
        $file_name = $user->id . '.' . $extension;

        $path = public_path('assets/img/user/' . $file_name);

        Image::make($request->file('file'))
            ->fit(144, 144)
            ->save($path);

        $user->avatar = $request->root() . "/assets/img/user/" . $file_name;
        $user->save();

        $data['success'] = true;
        $data['file_name'] = $request->root() . "/assets/img/user/" . $file_name;
        return $data;
    }
    public function updatePassword(Request $request)
    {
        $user = User::where('id', auth()->user()->id)
            ->get();
        $clave = $request->actualContraseña;

        if (Hash::check($clave, $user[0]->password)) {
            $data['success'] = true;
            User::where('id', auth()->user()->id)
                ->update([
                    'password' => Hash::make($request->contraseña)
                ]);
        } else {
            $data['success'] = false;
        }
        return $data;
    }
}
