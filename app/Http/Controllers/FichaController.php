<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\carbon;
use App\Session;
use App\comentarios;
use App\Diagnostico;
use App\Manual;
use App\Observacion;
use App\Paciente_profesional;
use App\Sesion;
use Barryvdh\DomPDF\Facade as PDF;


class FichaController extends Controller
{
    //Consulta de vista 'information.blade' para retornar datos de pacientes
    public function informationPaciente($id)
    {
        $paciente = User::find($id);
        $diagnostico = Diagnostico::find($id);$manual = new Manual;
        return view('ficha.information', compact('paciente','diagnostico'));
    }

    //Consulta de vista 'create.blade' para retornar datos de pacientes
    public function crearDiagnostico($id)
    {
        //$pacientes = \DB::table('users')->select('id','rut','name','apellido','email')->get();
        $paciente = User::find($id);
        $diagnostico = Diagnostico::find($id);
        $manuales = Manual::where('id_ficha', $id)->get();
        return view('ficha.create', compact('paciente', 'diagnostico', 'manuales'));
    }

    //Consulta de vista 'show.blade' para retornar datos de pacientes
    public function verDiagnostico($id)
    {
        //$pacientes = \DB::table('users')->select('id','rut','name','apellido','email')->get();
        $paciente = User::find($id);
        $diagnostico = Diagnostico::find($id);
        $manuales = Manual::where('id_ficha', $id)->get();
        $manuales1 = Manual::select('eje1', 'eje2', 'eje3', 'eje4', 'eje5')
        ->from('manual')
        ->where('id_ficha', '=', $id)
        ->where('tipo_manual','=', 1)
        ->get();
        $manuales2 = Manual::select('eje1', 'eje2', 'eje3', 'eje4', 'eje5', 'eje6')
        ->from('manual')
        ->where('id_ficha', '=', $id)
        ->where('tipo_manual','=', 2)
        ->get();
        $manuales3 = Manual::select('eje1', 'eje2', 'eje3')
        ->from('manual')
        ->where('id_ficha', '=', $id)
        ->where('tipo_manual','=', 3)
        ->get();
        $observaciones = Observacion::where('id_ficha', $id)->get();
        return view('ficha.show', compact('paciente', 'diagnostico', 'observaciones', 'manuales', 'manuales1', 'manuales2', 'manuales3'));
    }

    // Método para buscar y ordenar listado de pacientes
    public function buscarPaciente(Request $request, $id)
    {
        // ->where('id_profesional', $id)
        $profesional = $id;
        $name = $request->get('name');
        $rut = $request->get('rut');
        $id = $request->get('id');
        $estado = $request->get('estado');
        $pacientes = User::where('tipo', "Paciente")->orderBy('id', 'ASC')
            // nombres -> name
            ->name($name)
            // Ruts -> rut
            ->rut($rut)
            ->id($id)
            ->estado($estado)
            ->paginate(5);
        return view('ficha.pacientes', compact('pacientes', 'profesional'));
    }
    
    public function guardarDiagnostico($id)
    {
        // Método para almacenar diagnóstico en base de datos
        $paciente = User::find($id);
        $diagnostico = new Diagnostico;
        $diagnostico->id = $id;
        $diagnostico->descripcion = request()->txt_nuevo_diagnostico;
        $diagnostico->id_ficha = $id;
        // alert
        if ($diagnostico->save()) {
            return redirect()->route('create', compact('paciente', 'diagnostico', 'manuales'))->with('successgd', 'Diagnóstico ingresado correctamente.');
            //return view('ficha.create', compact('paciente', 'diagnostico', 'manuales'))->with('successgd', 'Diagnóstico ingresado correctamente.');
        } elseif (!$diagnostico->save()) {
            return view('ficha.create', compact('paciente', 'diagnostico', 'manuales'))->with('successgd', 'Diagnóstico NO ingresado.');
        } else {
            return back();
        }
    }

    public function updateDiagnostico(Request $request, $id)
    {
        // Método utilizado para actualizar diagnóstico
        $diagnosticoUpdate = Diagnostico::findOrFail($id);
        $diagnosticoUpdate->descripcion = $request->txt_editar_diagnostico;
        // alert
        if ($diagnosticoUpdate->save()) {
            return back()->with('success', 'Datos actualizados!');
        } elseif (!$diagnosticoUpdate->save()) {
            return back()->with('warning', 'Error al actualizar datos!');
        } else {
            return back();
        }
    }

    public function updateManual(Request $request, $id)
    {
        // Método utilizado para actualizar primer manual
        $manualUpdate = Manual::find($id);
        $manualUpdate->eje1 = $request->txt_editar_eje1;
        $manualUpdate->eje2 = $request->txt_editar_eje2;
        $manualUpdate->eje3 = $request->txt_editar_eje3;
        $manualUpdate->eje4 = $request->txt_editar_eje4;
        $manualUpdate->eje5 = $request->txt_editar_eje5;
        // alert
        if ($manualUpdate->save()) {
            return back()->with('successm1', 'Datos actualizados!');
        } elseif (!$manualUpdate->save()) {
            return back()->with('warningm1', 'Error al actualizar datos!');
        } else {
            return back();
        }
    }

    public function updateManual2(Request $request, $id)
    {
        // Método utilizado para actualizar segundo manual
        $manualUpdate = Manual::findOrFail($id);
        $manualUpdate->eje1 = $request->txt_editar_eje1;
        $manualUpdate->eje2 = $request->txt_editar_eje2;
        $manualUpdate->eje3 = $request->txt_editar_eje3;
        $manualUpdate->eje4 = $request->txt_editar_eje4;
        $manualUpdate->eje5 = $request->txt_editar_eje5;
        $manualUpdate->eje6 = $request->txt_editar_eje6;
        // alert
        if ($manualUpdate->save()) {
            return back()->with('successm2', 'Datos actualizados!');
        } elseif (!$manualUpdate->save()) {
            return back()->with('warningm2', 'Error al actualizar datos!');
        } else {
            return back();
        }
    }

    public function updateManual3(Request $request, $id)
    {
        // Método utilizado para actualizar tercer manual
        $manualUpdate = Manual::findOrFail($id);
        $manualUpdate->eje1 = $request->txt_editar_eje1;
        $manualUpdate->eje2 = $request->txt_editar_eje2;
        $manualUpdate->eje3 = $request->txt_editar_eje3;
        // alert
        if ($manualUpdate->save()) {
            return back()->with('successm3', 'Datos actualizados!');
        } elseif (!$manualUpdate->save()) {
            return back()->with('warningm3', 'Error al actualizar datos!');
        } else {
            return back();
        }
    }

    public function guardarManual($id)
    {
        // Método para almacenar manual diagnóstico en base de datos
        $paciente = User::find($id);
        $manual = new Manual;
        $manual->tipo_manual = request()->tipo_oculto;
        $manual->eje1 = request()->txt_evaluacion_eje1;
        $manual->eje2 = request()->txt_evaluacion_eje2;
        $manual->eje3 = request()->txt_evaluacion_eje3;
        $manual->eje4 = request()->txt_evaluacion_eje4;
        $manual->eje5 = request()->txt_evaluacion_eje5;
        $manual->eje6 = request()->txt_evaluacion_eje6;
        $manual->id_ficha = 8 ; //Modificar esta linea para que reconozca el id de la ficha actual (id_ficha = diagnostico->id = users->id)
        $manual->save();
        if ($manual->save()) {
            return back()->with('successm3', 'Datos guardados correctamente!');

        } elseif (!$manual->save()) {
            return back()->with('warningm3', 'Error al guardar los datos!');
        } else {
            return back();
        }        
    }

    public function guardarComentario($id)
    {
        // Método para almacenar comentario en base de datos
        $paciente = User::find($id);
        $observacion = new Observacion;
        $observacion->observacion = request()->txt_nuevo_comentario;
        $observacion->id_ficha = $id;
        $observacion->diagnostico_id = request()->idDiag;
        $observacion->save();
        $diagnostico = Diagnostico::find($id);
        $manuales = Manual::where('id_ficha', $id)->get();
        $observaciones = Observacion::where('id_ficha', $id)->get();
        // alert
        if ($observacion->save()) {
            return redirect()->route('show', ['id' => $id]); 
        } elseif (!$observacion->save()) {
            return redirect()->route('show', ['id' => $id]); 
        } else {
            return view('ficha.show', compact('paciente', 'diagnostico', 'observaciones', 'manuales'));
        }
    }

    public function egresarPaciente($id)
    {
        // Método creado para retornar la vista egresar paciente
        $paciente = User::find($id);
        return view('ficha.egresarPaciente', compact('paciente'));
    }

    public function altaPaciente(Request $request, $id)
    {
        // Método creado para dar de alta a un paciente
        $pacienteAlta = User::findOrFail($id);
        $pacienteAlta->tipo_alta = $request->rd_egreso;
        $fecha_egreso = Carbon::now();
        $pacienteAlta->fecha_egreso = $fecha_egreso;
        $pacienteAlta->estado = 2;
        $pacienteAlta->save();
        return back();
    }

    public function verSesiones($id, Request $request)
    {
        // Método creado para la función de egresar a un paciente
        $paciente = User::find($id);
        $sesiones = Sesion::where('id_ficha', $id)->get();
        $number = $request->get('number');
        $descrip = $request->get('descrip');
        $fecha = $request->get('fecha');
        $sesiones = Sesion::where('id_ficha', $id)
            ->orderBy('id', 'ASC')
            ->number($number)
            ->descrip($descrip)
            ->fecha($fecha)
            ->paginate(4);
        return view('ficha.sesiones', compact('paciente', 'sesiones'));
    }

    // Método para Descargar PDF con los datos del diagnóstico
    public function downloadPdf($id)
    {
        $paciente = User::findOrFail($id);
        $diagnostico = Diagnostico::find($id);
        $pdf = PDF::loadView('ficha.pdf', compact('paciente', 'diagnostico'));
        return $pdf->download('diagnostico.pdf');
    }

    // Método para Descargar la Ficha médica del paciente
    public function downloadFicha($id)
    {
        $paciente = User::findOrFail($id);
        $diagnostico = Diagnostico::find($id);
        if(!isset($diagnostico)){
            //return redirect()->back()->with('alert','No hay diagnósticos creados');
            //return redirect()->back()->withErrors(['msg', 'No hay dianósticos creados']);
            return redirect()->back()->withErrors(['name' => 'No hay diagnósticos creados']);
        }else{
            $manuales = Manual::where('id_ficha', $id)->get();
            $sesiones = Sesion::where('id_ficha', $id)->get();
            $observaciones = Observacion::where('id_ficha', $id)->get();
            $pdf = PDF::loadView(
                'ficha.fichaMedica',
                compact('paciente', 'diagnostico', 'manuales', 'sesiones', 'observaciones')
            );
            return $pdf->download('ficha_paciente.pdf');
        }
    }
    public function guardarSesion($id)
    {
        // Método para almacenar una nueva sesión en base de datos
        $paciente = User::find($id);

       
        $sesionReferencia = Sesion::max('numero_sesion'); // contiene el        
        if ($sesionReferencia == null){
            $sesionReferencia = 0;
        }
        $sesion = new Sesion;
        $sesion->id_ficha = $id;
        $sesion->descripcion = request()->txt_descripcion_sesion;
        $sesion->fecha = request()->txt_fecha_sesion;
        $sesion->numero_sesion = $sesionReferencia + 1;
        $sesion->periodo = request()->txt_periodo;
        $sesiones = Sesion::where('id_ficha', $id)->get();
        
        // alert
        if ($sesion->save()) {
            return back()->with('successse', 'Sesión Ingresada Correctamente.');
        } elseif (!$sesion->save()) {
            return view(
                'ficha.sesiones',
                compact('paciente', 'id', 'sesion', 'sesiones')
            )->with('warningse', 'Error al ingresar Sesión.');
        } else {
            return back();
        }
        
    }

    public function informationManuales($id)
    {
        $paciente = User::find($id);
        $diagnostico = Diagnostico::find($id);
        $manuales = Manual::where('id_ficha', $id)->get();
        $observaciones = Observacion::where('id_ficha', $id)->get();
        return view('ficha.nuevomanual', compact('paciente', 'diagnostico', 'observaciones', 'manuales'));
    }





}
