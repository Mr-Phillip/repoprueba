<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Socialite;
use App\User;
 
class SocialLoginController extends Controller
{
 
    public function redirect($provider) {
        return Socialite::driver($provider)->redirect();
    }
 
    public function callback($provider) {
        $getInfo = Socialite::driver($provider)->user()->user;
        $user = User::where('provider_id', $getInfo['id'])->first();
        if (!$user) {
            $user = User::create([
                'name' => $getInfo['given_name'],
                'apellido' => $getInfo['family_name'],
                'avatar' => $getInfo['picture'],
                'email' => $getInfo['email'],
                'tipo'=>'Paciente',
                'provider' => $provider,
                'provider_id' => $getInfo['id']
            ]);
        }
        
        auth()->login($user);
        return redirect()->to('/');
    }
}
