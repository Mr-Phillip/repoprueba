<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoProfesional extends Model
{
    protected $table='info_profesional';
    protected $primary_id='id';
    protected $foreign_key='user_id';
    

    protected $fillable = [
        'id',
        'titulo', 
        'especialidad',
        'descripcion',
        'fecha_egreso',
        'fecha_nacimiento',
        'institucion',
        'edad',
        'experiencia',
        'imagen_titulo',
        'user_id',
        'created_at', 
        'updated_at',
        'hora_inicio',
        'hora_termino',
        'hora_almuerzo',
    ];
}
