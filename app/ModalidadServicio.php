<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModalidadServicio extends Model
{
    protected $table = "modalidad_servicio";
    protected $primary_key = "id";
    public $timestamps = false;

    protected $fillable = [
        'id',
        'presencial',
        'online',
        'visita',
        'servicio_id',
    ];
}
