<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observacion extends Model
{
    //Modelo utilizado para el almacenamiento de comentarios que realiza un profesional para cada diagnóstico
    protected $table='observacion';
    protected $primary_key='id';

    protected $fillable=[
        'id',
        'observacion',
        'created_at',
        'updated_at',
    ];
}
