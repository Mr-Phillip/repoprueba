<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente_profesional extends Model
{
    protected $table='paciente_profesional';
    protected $primary_key='id';

    protected $fillable=[
        'id',
        'id_paciente',
        'id_profesional',
    ];
}
