<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table = "servicio";
    protected $primary_key = "id";
    public $timestamps = false;

    protected $fillable = [
        'id',
        'nombre', 
        'descripcion',
        'duracion',
        'user_id',
    ];
    
    public function modalidad(){
        return $this->hasMany('App\ModalidadServicio');
    }

    public function precio(){
        return $this->hasMany('App\PrecioServicio');
    }

    public function isapre(){
        return $this->hasMany('App\IsapreServicio');
    }
    public function cita(){
        return $this->hasMany('App\Cita');
    }
}
