<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sesion extends Model
{
    //Modelo utilizado para el almacenamiento de comentarios que realiza un profesional para cada diagnóstico
    protected $table='sesion';
    protected $primary_key='id';

    protected $fillable=[
        'id',
        'id_ficha',
        'descripcion',
        'fecha',
        'numero_sesion',
        'created_at',
        'updated_at',
        'periodo',
    ];
    
    public function scopeNumber($query, $number)
    {
        if($number)
            return $query->where('numero_sesion', $number);

    }

    public function scopeDescrip($query, $descrip)
    {
        if ($descrip)
            return $query->where('descripcion', 'LIKE', "%$descrip%");
    }

    public function scopeFecha($query, $fecha)
    {
        if ($fecha)
            return $query->where('fecha', 'LIKE', "%$fecha%");
    }
}
