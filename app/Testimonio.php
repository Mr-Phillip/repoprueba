<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonio extends Model
{
    protected $table = 'testimonio';
    protected $primaryKey = 'id';

    protected $fillable = [
        'valoracion','titulo','comentario','paciente_id','profesional_id', 'estado', 'anonimo'
    ];
    
    public function user() {
        return $this->belongsTo('App\User','paciente_id');
    }
}
