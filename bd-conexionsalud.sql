-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-09-2020 a las 22:28:38
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd-psicologos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` varchar(511) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`, `descripcion`, `created_at`) VALUES
(1, 'Categoria 1', 'Descripcion de categoria 1', '2020-08-25 00:00:00'),
(2, 'Categoria 2', 'Descripcion de categoria 3', '2020-08-25 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro`
--

CREATE TABLE `centro` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centro_users`
--

CREATE TABLE `centro_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `centro_id` int(11) NOT NULL,
  `fehca` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE `cita` (
  `id` int(11) NOT NULL,
  `locacion` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_termino` time DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `estado` varchar(45) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `servicio_id` int(11) NOT NULL,
  `modalidad` varchar(255) DEFAULT NULL,
  `descanso` time DEFAULT NULL,
  `prevision` varchar(45) DEFAULT NULL,
  `isapre` varchar(45) DEFAULT NULL,
  `precio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cita`
--

INSERT INTO `cita` (`id`, `locacion`, `fecha`, `hora_inicio`, `hora_termino`, `descripcion`, `estado`, `user_id`, `servicio_id`, `modalidad`, `descanso`, `prevision`, `isapre`, `precio`) VALUES
(1, NULL, '2020-09-17', '09:00:00', '09:00:00', NULL, 'Pendiente', 1, 36, 'Presencial', NULL, 'Fonasa', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `id` int(11) NOT NULL,
  `comentario` varchar(255) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`id`, `comentario`, `post_id`, `user_id`, `created_at`, `updated_at`, `estado`) VALUES
(1, 'Comentario para post de categoria 2', 4, 1, NULL, NULL, NULL),
(3, 'sdsdsdsdsd', 3, 1, '2020-09-11 19:50:21', '2020-09-11 22:50:21', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnostico`
--

CREATE TABLE `diagnostico` (
  `id` int(11) NOT NULL,
  `tipo_diagnostico_id` int(11) NOT NULL,
  `manual_id` int(11) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `diagnosticocol` varchar(45) DEFAULT NULL,
  `cita_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `info_profesional`
--

CREATE TABLE `info_profesional` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `modalidad_atencion` varchar(255) DEFAULT NULL,
  `especialidad` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `fecha_egreso` varchar(255) DEFAULT NULL,
  `fecha_nacimiento` varchar(25) DEFAULT NULL,
  `institucion` varchar(255) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `experiencia` int(11) DEFAULT NULL,
  `imagen_titulo` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `info_profesional`
--

INSERT INTO `info_profesional` (`id`, `titulo`, `modalidad_atencion`, `especialidad`, `descripcion`, `fecha_egreso`, `fecha_nacimiento`, `institucion`, `edad`, `experiencia`, `imagen_titulo`, `user_id`, `created_at`, `updated_at`) VALUES
(3, 'Psicólogo', 'Online', NULL, 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil aperiam eius similique pariatur dolorem.', '20-03-2008', '01-06-1984', 'Universidad Autónoma de Chile', 36, 5, NULL, 2, '2020-09-02 21:57:09', '2020-09-02 21:57:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `isapre_servicio`
--

CREATE TABLE `isapre_servicio` (
  `id` int(11) NOT NULL,
  `banmedica` int(11) DEFAULT NULL,
  `consalud` int(11) DEFAULT NULL,
  `colmena` int(11) DEFAULT NULL,
  `cruzblanca` int(11) DEFAULT NULL,
  `masvida` int(11) DEFAULT NULL,
  `vidatres` int(11) DEFAULT NULL,
  `servicio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `isapre_servicio`
--

INSERT INTO `isapre_servicio` (`id`, `banmedica`, `consalud`, `colmena`, `cruzblanca`, `masvida`, `vidatres`, `servicio_id`) VALUES
(2, NULL, NULL, NULL, NULL, NULL, NULL, 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manual`
--

CREATE TABLE `manual` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensaje`
--

CREATE TABLE `mensaje` (
  `id` int(11) NOT NULL,
  `mensaje` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `sala_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_08_17_172009_create_social_google_accounts_table', 1),
(3, '2020_08_17_185811_add_socialite_fields_to_users_table', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modalidad_servicio`
--

CREATE TABLE `modalidad_servicio` (
  `id` int(11) NOT NULL,
  `presencial` int(11) DEFAULT '0',
  `online` int(11) DEFAULT '0',
  `visita` int(11) DEFAULT '0',
  `servicio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modalidad_servicio`
--

INSERT INTO `modalidad_servicio` (`id`, `presencial`, `online`, `visita`, `servicio_id`) VALUES
(2, NULL, NULL, 1, 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `observacion`
--

CREATE TABLE `observacion` (
  `id` int(11) NOT NULL,
  `diagnostico_id` int(11) NOT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_paciente`
--

CREATE TABLE `pago_paciente` (
  `id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `tipo_pago` varchar(255) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  `cita_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_profesional`
--

CREATE TABLE `pago_profesional` (
  `id` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `tipo_pago` varchar(255) DEFAULT NULL,
  `monto` int(11) DEFAULT NULL,
  `suscripcion_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `contenido` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `categoria_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `post`
--

INSERT INTO `post` (`id`, `titulo`, `contenido`, `imagen`, `created_at`, `estado`, `categoria_id`, `user_id`) VALUES
(3, 'Post 1', 'Contenido post 1', NULL, NULL, NULL, 1, 1),
(4, 'Post categoria 2', 'Contenido del post categoria 2', NULL, NULL, NULL, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post_users`
--

CREATE TABLE `post_users` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `precio_servicio`
--

CREATE TABLE `precio_servicio` (
  `id` int(11) NOT NULL,
  `precioFonasa` int(11) DEFAULT '0',
  `precioIsapre` int(11) DEFAULT '0',
  `precioParticular` int(11) DEFAULT '0',
  `servicio_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `precio_servicio`
--

INSERT INTO `precio_servicio` (`id`, `precioFonasa`, `precioIsapre`, `precioParticular`, `servicio_id`) VALUES
(2, NULL, NULL, 25000, 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sala`
--

CREATE TABLE `sala` (
  `id` int(11) NOT NULL,
  `paciente_id` int(11) NOT NULL,
  `psicologo_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `duracion` time DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id`, `nombre`, `descripcion`, `duracion`, `user_id`) VALUES
(37, 'Servicio 1', 'Prueba de servicio 1', '00:30:00', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `suscripcion`
--

CREATE TABLE `suscripcion` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `testimonio`
--

CREATE TABLE `testimonio` (
  `id` int(11) NOT NULL,
  `valoracion` int(11) DEFAULT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `anonimo` tinyint(4) DEFAULT NULL,
  `comentario` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL,
  `profesional_id` int(11) NOT NULL,
  `paciente_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `testimonio`
--

INSERT INTO `testimonio` (`id`, `valoracion`, `titulo`, `anonimo`, `comentario`, `updated_at`, `created_at`, `estado`, `profesional_id`, `paciente_id`) VALUES
(1, 4, 'Testimonio de prueba', 1, 'Probando los testimonios', '2020-09-02 17:21:50', '2020-09-02 17:21:50', NULL, 1, 1),
(2, 4, 'Testimonio de prueba', 1, 'Comentario de prueba', '2020-09-02 17:58:35', '2020-09-02 17:58:35', NULL, 2, 1),
(3, 4, 'SDFAF', 1, 'CVVC', '2020-09-03 13:49:24', '2020-09-03 13:49:24', NULL, 2, 1),
(4, 4, 'sdas', 0, 'sdsd', '2020-09-03 14:36:22', '2020-09-03 14:36:22', NULL, 2, 1),
(5, 3, 'dasdad', 0, 'fdfdfadfaf', '2020-09-03 21:08:21', '2020-09-03 21:08:21', NULL, 2, 1),
(6, 1, 'de prueba', 1, 'probando', '2020-09-03 21:14:24', '2020-09-03 21:14:24', NULL, 2, 1),
(7, 4, 'sdsd', 0, 'sdsd', '2020-09-04 12:54:08', '2020-09-04 12:54:08', NULL, 2, 1),
(8, 5, 'das', 1, 'dsd', '2020-09-04 13:12:35', '2020-09-04 13:12:35', NULL, 2, 1),
(9, 5, 'dfdf', 0, 'dfdf', '2020-09-04 13:47:38', '2020-09-04 13:47:38', NULL, 2, 1),
(10, 5, 'dsd', 0, 'dsd', '2020-09-04 22:10:20', '2020-09-04 22:10:20', NULL, 2, 1),
(11, 5, 'edfdf', 0, 'dfdsf', '2020-09-06 20:19:42', '2020-09-06 20:19:42', NULL, 2, 1),
(12, 5, 'df', 0, 'df', '2020-09-06 20:20:09', '2020-09-06 20:20:09', NULL, 2, 1),
(13, 5, 'dfd', 1, 'df', '2020-09-06 20:20:18', '2020-09-06 20:20:18', NULL, 2, 1),
(14, 5, 'dsfd', 1, 'dfd', '2020-09-06 20:28:38', '2020-09-06 20:28:38', NULL, 2, 1),
(15, 5, 'Prueba de testimonios', 0, 'sdasdfdfdfasdfasd', '2020-09-10 20:32:06', '2020-09-10 20:32:06', NULL, 2, 1),
(16, 5, 'dfdf', 1, 'dfdf', '2020-09-10 20:32:59', '2020-09-10 20:32:59', NULL, 2, 1),
(17, 4, 'ddfdf', 0, 'dfdfdf', '2020-09-10 20:35:11', '2020-09-10 20:35:11', NULL, 2, 1),
(18, 4, 'dfdfdf', 1, 'sfdfd', '2020-09-10 20:36:26', '2020-09-10 20:36:26', NULL, 2, 1),
(19, 5, 'dfdfsdf', 0, 'dfdfdfdf', '2020-09-10 20:38:25', '2020-09-10 20:38:25', NULL, 2, 1),
(20, 5, 'dfdfsdfsdf', 1, 'dfdfdfdf', '2020-09-10 20:41:51', '2020-09-10 20:41:51', NULL, 2, 1),
(21, 5, 'sdsdsd', 0, 'sdsdsdsd', '2020-09-10 20:46:55', '2020-09-10 20:46:55', NULL, 2, 1),
(22, 5, 'dfdfd', 1, 'dfdfdf', '2020-09-10 20:47:57', '2020-09-10 20:47:57', NULL, 2, 1),
(23, 5, 'dfdf', 0, 'dfdfdfdf', '2020-09-10 20:58:10', '2020-09-10 20:58:10', NULL, 2, 1),
(24, 5, 'dfdf', 1, 'dfdfdf', '2020-09-10 21:03:27', '2020-09-10 21:03:27', NULL, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_diagnostico`
--

CREATE TABLE `tipo_diagnostico` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `tipo_diagnosticocol` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `nickname` varchar(45) DEFAULT NULL,
  `rut` varchar(10) DEFAULT NULL,
  `telefono` varchar(12) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `comuna` varchar(255) DEFAULT NULL,
  `provider` varchar(191) DEFAULT NULL,
  `provider_id` varchar(191) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `apellido`, `email`, `password`, `avatar`, `nickname`, `rut`, `telefono`, `fecha_nacimiento`, `tipo`, `direccion`, `comuna`, `provider`, `provider_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Claudio', 'Narvaez', 'claudionarvaez61@gmail.com', NULL, 'https://lh3.googleusercontent.com/a-/AOh14GiV1MFYGlnmwjpSaZkvErPclK-zBXGxkPDquFWN', NULL, NULL, NULL, NULL, 'Paciente', NULL, NULL, 'google', '111774136020669051700', NULL, '2020-09-02 21:38:01', '2020-09-02 21:38:01'),
(2, 'Sergio', 'Pino', 'sergio.pino@services.lef', '$2y$10$//FYeXyrnVDrCmJdGOOH.ekZeURQeEg2fGlHdRX1fhA03ueSW6oLO', NULL, NULL, '15777562-6', '12345678', NULL, 'Profesional', 'Recinto Feria', 'Temuco', NULL, NULL, NULL, '2020-09-02 21:40:13', '2020-09-02 21:56:59');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `centro`
--
ALTER TABLE `centro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `centro_users`
--
ALTER TABLE `centro_users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cita`
--
ALTER TABLE `cita`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `info_profesional`
--
ALTER TABLE `info_profesional`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `isapre_servicio`
--
ALTER TABLE `isapre_servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `manual`
--
ALTER TABLE `manual`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modalidad_servicio`
--
ALTER TABLE `modalidad_servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `observacion`
--
ALTER TABLE `observacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pago_paciente`
--
ALTER TABLE `pago_paciente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pago_profesional`
--
ALTER TABLE `pago_profesional`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `post_users`
--
ALTER TABLE `post_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_idx` (`user_id`),
  ADD KEY `post_id_idx` (`post_id`);

--
-- Indices de la tabla `precio_servicio`
--
ALTER TABLE `precio_servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sala`
--
ALTER TABLE `sala`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `testimonio`
--
ALTER TABLE `testimonio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_diagnostico`
--
ALTER TABLE `tipo_diagnostico`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `centro`
--
ALTER TABLE `centro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `centro_users`
--
ALTER TABLE `centro_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cita`
--
ALTER TABLE `cita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `info_profesional`
--
ALTER TABLE `info_profesional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `isapre_servicio`
--
ALTER TABLE `isapre_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `manual`
--
ALTER TABLE `manual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `mensaje`
--
ALTER TABLE `mensaje`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `modalidad_servicio`
--
ALTER TABLE `modalidad_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `observacion`
--
ALTER TABLE `observacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pago_paciente`
--
ALTER TABLE `pago_paciente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pago_profesional`
--
ALTER TABLE `pago_profesional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `post_users`
--
ALTER TABLE `post_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `precio_servicio`
--
ALTER TABLE `precio_servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `sala`
--
ALTER TABLE `sala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT de la tabla `suscripcion`
--
ALTER TABLE `suscripcion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `testimonio`
--
ALTER TABLE `testimonio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `tipo_diagnostico`
--
ALTER TABLE `tipo_diagnostico`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `post_users`
--
ALTER TABLE `post_users`
  ADD CONSTRAINT `post_id` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
