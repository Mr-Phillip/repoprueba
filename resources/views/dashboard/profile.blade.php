@extends('layouts.dashboard')

@section('contentSidebar')
@switch(auth()->user()->tipo)
@case("Profesional")
@include('dashboard.profile.profileProfesional')
@break
@case("Paciente")
@include('dashboard.profile.profilePaciente')
@break
@endswitch
@endsection