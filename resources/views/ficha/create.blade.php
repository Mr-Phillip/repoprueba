@extends('layouts.dashboard')
@section('contentSidebar')
<!-- INICIO MENÚ 'DIAGNÓSTICO PSICOLÓGICO'-->
<div class="container_information">
    <div class="row_diagnostico_manual">
        <div class="col">
            <a href="{{route('information', $paciente->id)}}" class="btn btn-danger stretched-link">Volver</a>
        </div>
        <div class="col">
            <h3 class="card-title">Diagnósticos Psicológicos</h3>
        </div>
        <div class="col">
            <h5>Paciente : {{$paciente->name}} {{$paciente->apellido}}</h5>
            <h5>N° Ficha : {{$paciente->id}}</h5>
        </div>
    </div>
</div>
<!-- TÉRMINO MENÚ 'DIAGNÓSTICO PSICOLÓGICO' -->

@if(@isset($diagnostico))
<div class="card text-center container_information">
    <div class="card-body">
        <h3 class="card-title">Diagnóstico creado</h3>
        <a href="{{route('show', $paciente->id)}}" class="btn btn-primary stretched-link">Ver Diagnóstico</a>
    </div>
</div>
@else

<!-- INICIO DESCRIPCIÓN DIAGNÓSTICO PSICOLÓGICO -->
<div class="card text-center container_information">
    <div class="card-body">
        <h3 class="card-title">Crear nuevo Diagnóstico psicológico</h3>
        <p> Si desea utilizar un manual, puede crear uno mas abajo en esta misma pagina. <br>
            (Si ya tiene creado un manual, por favor indique en el diagnostico que utilizará dicho manual).</p>
        <form action="{{route('guardarDiagnosticoBD', $paciente->id)}}" method="post" role="form">
            @csrf
            <div class="form-group">
                <textarea class="form-control" id="txt_nuevo_diagnostico" name="txt_nuevo_diagnostico" rows="8"
                    required></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Guardar Diagnóstico</button>
        </form>
    </div>
</div>
<!-- TÉRMINO DESCRIPCIÓN DIAGNÓSTICO PSICOLÓGICO -->
@endif

<!-- Consulta por si se recibe la variable manuales, (si existe un manual ingresado en base de datos) -->
<div class="card container_information">
    <div class="card-body">
        @if($manuales->count()!==0)
        <h3 class="card-title">Manuales creados:{{$manuales->count()}}</h3>
        <div class="row">
            <div class="col-md-8">
                <br>
                @if(!isset($diagnostico))
                No hay diagnóstico creado.
                </script>
                <div class="alert alert-success" role="alert">Recuerde indicar que utilizará un manual.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @else
                    info manual
                @endif
            </div>
        </div>
        @else
        <h3 class="card-title">Manuales creados:{{$manuales->count()}}</h3>
        <div class="row">
            <div class="col-md-8">
                <br>
                <a href="{{route('newmanual', $paciente->id)}}" class="btn btn-primary stretched-link"
                    style="width: 100%;">Crear Manuales</a>
            </div>
        </div>
        @endif
    </div>
</div>

@endsection