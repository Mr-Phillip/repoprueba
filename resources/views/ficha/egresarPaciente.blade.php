@extends('layouts.dashboard')
@section('contentSidebar')
<!-- INICIO MENÚ 'EGRESAR PACIENTE'-->

<!-- Encabezado -->
<div class="container_information">
    <div class="row_diagnostico_manual">
        <div class="col">
            <a href="{{route('information', $paciente->id)}}" class="btn btn-danger stretched-link">Volver</a>
        </div>
        <div class="col">
            <h3 class="card-title">Menú egreso de pacientes</h3>
        </div>
        <div class="col">
            <h5>Paciente : {{$paciente->name}} {{$paciente->apellido}}</h5>
            <h5>N° Ficha : {{$paciente->id}}</h5>
        </div>
    </div>
</div>
<!-- Encabezado -->

<div class="card text-center container_information">
    <form action="{{route('alta-Paciente', $paciente->id)}}" method="post" role="form">
        @method('PUT')
        @csrf
        <div class="card-body">
            <h3 class="card-title">¿Desea egresar al paciente <strong>{{$paciente->name}} {{$paciente->apellido}}</strong>?</h3>
            <h4>Ingrese el motivo de egreso</h4>

            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="1" name="rd_egreso" class="custom-control-input" value="1">
                <label class="custom-control-label" for="1">Alta Terapéutica</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="2" name="rd_egreso" class="custom-control-input" value="2">
                <label class="custom-control-label" for="2">Alta Administrativa</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline color_radio">
                <input type="radio" id="3" name="rd_egreso" class="custom-control-input" value="3">

                <label class="custom-control-label" for="3">Abandono</label>
            </div>

        </div>
        <button type="submit" class="btn btn-danger" id="egresar_paciente" name="btn_egresar_paciente">Egresar Paciente</button>
    </form>
</div>


<!-- TÉRMINO MENÚ EGRESAR PACIENTE -->
@endsection