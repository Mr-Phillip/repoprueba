<!-- Incorporar contenido de vista dashboard -->
@extends('layouts.dashboard')

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
    integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
</script>



@section('contentSidebar')

<div class="container_information">
    <a class="btn btn-danger" href="{{ route('buscar', ['id' => auth()->user()]) }}">
        <i class="fas fa-arrow-left"> Volver</i>
    </a>
    <h2 class="title_info">Información Médica General</h2>
    <p class="text-5 darkgray-text col-12 text-center">Informacion de ficha clinica del paciente </p>
    {{-- <div id="divBtn" class="d-flex justify-content-center">
        <label id="mostrar_datos" class="col-1 text-5 text-bold indigo-text dropbtn d-flex"
            style="margin-bottom: 30px;">Ver Datos</label>
    </div> --}}
    <div class="ul sm-pr-0" id="paciente_datos">
        <div class="row info_paciente" style="min-height: 40vh;" >
            <div class="col-6 col-md-3 ctn_info_paciente">
                <div>
                    @if(auth()->user()->avatar)
                    <img src="{{$paciente->avatar}}" class="medium-avatar rounded-circle align-self-center"
                        alt="Icono Paciente">
                    @else
                    <img src="{{asset('assets/img/avatar.png')}}"
                        class="medium-avatar rounded-circle align-self-center">
                    @endif
                </div>
                <div class="row">
                    <div class="col">
                        <label for=""><b>N° Ficha:</b></label>
                        <label for="">{{$paciente->id}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for=""><b>Nombre completo:</b></label>
                        <label for="">{{$paciente->name}} {{$paciente->apellido}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for=""><b>Rut:</b></label>
                        <label for="">{{$paciente->rut}}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <a href="{{route('descargarFicha', $paciente->id)}}"
                            class="btn btn-danger " id="descargar_ficha"
                            name="descargar_ficha" style="width: 100%;">
                            Descargar Ficha
                        </a> 
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <a class="btn btn-info mb-1 mt-1" data-toggle="collapse" href="#pruebaprimerDiv" role="button" 
                            aria-expanded="false" aria-controls="pruebaprimerDiv" style="width: 100%;">
                            Info paciente.
                        </a> 
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <a class="btn btn-info mb-2 mt-1" data-toggle="collapse" href="#pruebasegundoDiv" role="button" 
                            aria-expanded="false" aria-controls="pruebasegundoDiv" style="width: 100%;">
                            Info clinica.
                        </a> 
                    </div>
                </div>
            </div>

            <div class="col-6 col-md-4 ctn_info_paciente collapse multi-collapse" id="pruebaprimerDiv">
                <label for="">F. nacimiento :</label>
                <input class="form-control" id="fnaci" type="text" value="{{$paciente->fecha_nacimiento}}"
                    readonly="readonly">
                <label for="">Estudios :</label>
                <input class="form-control" type="text" value="{{$paciente->estudios}}" readonly="readonly">
                <label for="">Correo :</label>
                <input class="form-control" type="text" value="{{$paciente->email}}" readonly="readonly">
                <label for="">Ocupación :</label>
                <input class="form-control" id="ocupacion" type="text" value="{{$paciente->ocupacion}}"
                    readonly="readonly">
            </div>
            <div class="col-6 col-md-4 ctn_info_paciente collapse multi-collapse" id="pruebasegundoDiv">
                <label for="">Estado :</label>
                <input class="form-control" type="text"
                    value=" @if ($paciente->estado === 1) Activo @else Histórico @endif " readonly="readonly">
                <label for="">Tipo de Egreso :</label>
                <input class="form-control" type="text"
                    value="@if ($paciente->tipo_alta === 1)Alta Terapéutica @elseif ($paciente->tipo_alta === 2)Alta Administrativa @elseif ($paciente->tipo_alta === 3) Abandono  @else  @endif"
                    readonly="readonly">
                <label for="">Fecha de Egreso :</label>
                <input class="form-control" type="text" value="{{$paciente->fecha_egreso}}" readonly="readonly">
            </div>
        </div>
        {{-- <div class="row info_paciente">
            <div class="col-6 col-md-4 ctn_info_paciente">
                <a href="{{route('descargarFicha', $paciente->id)}}"
                    class="btn btn-danger stretched-link btn-download-ficha" id="descargar_ficha"
                    name="descargar_ficha">Descargar Ficha paciente</a>
                @error('name')
                <p>{{ $message }}</p>
                @enderror
            </div>
        </div>  --}}
    </div>
</div>



<div class="container_information">
    <div class="row">
        @if(!isset($diagnostico->id))
        <div class="col-6 col-md-3 btn_accesos">
            <a href="{{route('create', $paciente->id)}}" class="btn btn-primary stretched-link"
                style="width: 100%;">Nuevo
                Diagnóstico</a>
        </div>
        @else
        <div class="col-6 col-md-3 btn_accesos">
            <a href="{{route('show', $paciente->id)}}" class="btn btn-primary stretched-link"
                style="width: 100%;">Ver Diagnóstico</a>
        </div>
        @endif
        <div class="col-6 col-md-3 btn_accesos">
            <a href="{{route('sesiones', $paciente->id)}}" class="btn btn-primary stretched-link"
                style="width: 100%;">Ver Sesiones</a>
        </div>
        <div class="col-6 col-md-3 btn_accesos">
            <a href="{{route('egresarPaciente', $paciente->id)}}" class="btn btn-success stretched-link"
                style="width: 100%;">Egresar
                paciente</a>
        </div>
    </div>
</div>

@endsection