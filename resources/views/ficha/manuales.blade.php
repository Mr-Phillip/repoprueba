<!-- INICIO EDITAR MANUALES DIAGNÓSTICOS -->

<!-- Validaciones -->

<div class="card text-center container_information ">

    <h3 class="card-title">Manuales diagnóstico psicológico</h3>
    <div class="description">
        <div class="evaluacion">
            <h4 class="titulo">DSM IV - TR</h4>
            <!-- EDITAR MANUAL -->
            <a class="btn btn-success btn_editar_manual1" data-toggle="collapse" href="#ctn_editar_manual1"
                role="button" aria-expanded="false" aria-controls="ctn_evaluacion1">Editar Evaluación
                Multiaxial</a>
            <div class="collapse multi-collapse ctn_comentario" id="ctn_editar_manual1">

                @foreach ($manuales as $manual)
                @if($manual->tipo_manual===1)
                <form action="{{route('updateManual',$manual->id)}}" method="post" role="form">
                    @method('PUT')
                    @csrf
                    <div class="evaluacion__1">
                        <label for="">Trastornos clinicos</label>
                        <textarea class="form-control" id="txt_editar_eje1" name="txt_editar_eje1" rows="2"
                            placeholder="Trastornos Clínicos" required>{{$manual->eje1}}</textarea>
                    </div>
                    <div class="evaluacion__1">
                        <label for="">Transtornos mentales</label>
                        <textarea class="form-control" id="txt_editar_eje2" name="txt_editar_eje2" rows="2"
                            placeholder="Trastornos de la persona" required>{{$manual->eje2}}</textarea>
                    </div>
                    <div class="evaluacion__1">
                        <label for="">Enfermedades medicas</label>
                        <textarea class="form-control" id="txt_editar_eje3" name="txt_editar_eje3" rows="2"
                            placeholder="Enfermedades Médicas" required>{{$manual->eje3}}</textarea>
                    </div>
                    <div class="evaluacion__1">
                        <label for="">Problemas psicosociales y ambientales</label>
                        <textarea class="form-control" id="txt_editar_eje4" name="txt_editar_eje4" rows="2"
                            placeholder="EAG (0-100)" required>{{$manual->eje4}}</textarea>
                    </div>
                    <div class="evaluacion__1">
                        <label for="">EEAG</label>
                        <textarea class="form-control" id="txt_editar_eje5" name="txt_editar_eje5" rows="2"
                            placeholder="EEAG (0-100)" required>{{$manual->eje5}}</textarea>
                    </div>
                    <div class="evaluacion__1">
                        <button type="submit" class="btn btn-primary" id="manual1" name="btn_manual1">Guardar
                            Cambios</button>
                    </div>
                </form>
                @else
                <div class="alert alert-success" role="alert">Este manual aun no está creado
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @endforeach


            </div>
            <!-- Alert -->
            @if(session()->has('successm1'))
            <div class="alert alert-success" role="alert">{{session('successm1')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <!-- Alert -->
        </div>

        <div class="evaluacion">
            <h4 class="titulo">CIE - 10</h4>
            <!-- EDITAR MANUAL -->
            <a class="btn btn-success btn_editar_manual2" data-toggle="collapse" href="#ctn_editar_manual2"
                role="button" aria-expanded="false" aria-controls="ctn_evaluacion1">Editar Evaluación
                Multiaxial</a>

            <div class="collapse multi-collapse ctn_comentario" id="ctn_editar_manual2">

                @foreach ($manuales as $manual)
                @if ($manual->tipo_manual===2)
                <form action="{{route('updateManual2', $manual->id)}}" method="post" role="form">
                    @method('PUT')
                    @csrf
                    <div class="evaluacion__1">
                        <label for="">Sindromes psiquiatricos</label>
                        <textarea class="form-control" id="txt_editar_eje1" name="txt_editar_eje1" rows="2"
                            placeholder="Evaluación global de la discapacidad" required>{{$manual->eje1}}</textarea>
                    </div>
                    <div class="evaluacion__1">
                        <label for="">Trastornos de desarrollo</label>
                        <textarea class="form-control" id="txt_editar_eje2" name="txt_editar_eje2" rows="2"
                            placeholder="Afectación del funcionamiento (WHODAS 2.0)"
                            required>{{$manual->eje2}}</textarea>
                    </div>
                    <div class="evaluacion__1">
                        <label for="">Nivel intelectual</label>
                        <textarea class="form-control" id="txt_editar_eje3" name="txt_editar_eje3" rows="2"
                            placeholder="Otras condiciones que puedan ser objeto de atención clínica"
                            required>{{$manual->eje3}}</textarea>
                    </div>
                    <div class="evaluacion__1">
                        <label for="">Condiciones medicas</label>
                        <textarea class="form-control" id="txt_editar_eje4" name="txt_editar_eje4" rows="2"
                            placeholder="Condiciones Médicas" required>{{$manual->eje4}}</textarea>
                    </div>
                    <div class="evaluacion__1">
                        <label for="">Situaciones psicosociales</label>
                        <textarea class="form-control" id="txt_editar_eje5" name="txt_editar_eje5" rows="2"
                            placeholder="Situaciones Psicosociales" required>{{$manual->eje5}}</textarea>
                    </div>
                    <div class="evaluacion__1">
                        <label for="">Evaluación de discapacidad</label>
                        <textarea class="form-control" id="txt_editar_eje6" name="txt_editar_eje6" rows="2"
                            placeholder="Evaluación Global de la Discapacidad" required>{{$manual->eje6}}</textarea>
                    </div>
                    <div class="evaluacion__1">
                        <button type="submit" class="btn btn-primary" id="manual2" name="btn_manual2">Guardar
                            Cambios</button>
                    </div>
                </form>
                @else
                <div class="alert alert-success" role="alert">Este manual aun no está creado
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @endforeach


            </div>
            <!-- Alert -->
            @if(session()->has('successm2'))
            <div class="alert alert-success" role="alert">{{session('successm2')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <!-- Alert -->
        </div>

        <div class="evaluacion">
            <h4 class="titulo">DSM V</h4>
            <!-- EDITAR MANUAL -->
            <a class="btn btn-success btn_editar_manual3" data-toggle="collapse" href="#ctn_editar_manual3"
                role="button" aria-expanded="false" aria-controls="ctn_evaluacion1">Editar Aproximación
                dimensional</a>

            <div class="collapse multi-collapse ctn_comentario" id="ctn_editar_manual3">

                @foreach ($manuales as $manual)
                @if($manual->tipo_manual===3)
                <form action="{{route('updateManual3', $manual->id)}}" method="post" role="form">
                    @method('PUT')
                    @csrf
                    <div class="evaluacion__1">
                        <label for="">Eje 1</label>
                        <textarea class="form-control" id="txt_editar_eje1" name="txt_editar_eje1" rows="2"
                            placeholder="Evaluación global de la discapacidad" required>{{$manual->eje1}}
                                </textarea>
                    </div>
                    <div class="evaluacion__1">
                        <label for="">Eje 2</label>
                        <textarea class="form-control" id="txt_editar_eje2" name="txt_editar_eje2" rows="2"
                            placeholder="Afectación del funcionamiento (WHODAS 2.0)" required>{{$manual->eje2}}
                                </textarea>
                    </div>
                    <div class="evaluacion__1">
                        <label for="">Eje 3</label>
                        <textarea class="form-control" id="txt_editar_eje3" name="txt_editar_eje3" rows="2"
                            placeholder="Otras condiciones que puedan ser objeto de atención clínica" required>{{$manual->eje3}}
                                </textarea>
                    </div>
                    <div class="evaluacion__1">
                        <button type="submit" class="btn btn-primary" id="manual3" name="btn_manual3">Guardar
                            Cambios</button>
                    </div>
                </form>
                @else
                <div class="alert alert-success" role="alert">Este manual aun no está creado
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @endforeach


            </div>
            <!-- Alert -->
            @if(session()->has('successm3'))
            <div class="alert alert-success" role="alert">{{session('successm3')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <!-- Alert -->
        </div>
    </div>
</div>
<!-- TÉRMINO EDITAR MANUALES DIAGNÓSTICOS -->