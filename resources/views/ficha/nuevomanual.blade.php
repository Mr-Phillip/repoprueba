@extends('layouts.dashboard')
@section('contentSidebar')
<div class="container_information">
    <div class="row_diagnostico_manual">
        <div class="col">
            <a href="{{route('information', $paciente->id)}}" class="btn btn-danger stretched-link">Volver</a>
        </div>
        <div class="col">
            <h3 class="card-title">Cración de manuales psicológicos</h3>
        </div>
        <div class="col">
            <h5>Paciente : {{$paciente->name}} {{$paciente->apellido}}</h5>
            <h5>N° Ficha : {{$paciente->id}}</h5>
        </div>
    </div>
</div>
<div class="card text-center container_information">
    <div class="card-body">
        <h4>Seleccione el tipo de manual:</h4>
        <br>
        <div class="row">
            <div class="col-md-4">
                <div class="form-check form-check-inline">
                <a class="btn btn-success" data-toggle="collapse" href="#form1"
                role="button" aria-expanded="false" aria-controls="ctn_evaluacion1" style="width: 100%;">DSM IV - TR</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-check form-check-inline">
                <a class="btn btn-success btn_editar_manual1" data-toggle="collapse" href="#form2"
                role="button" aria-expanded="false" aria-controls="ctn_evaluacion1" style="width: 100%;">CIE - 10</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-check form-check-inline">
                <a class="btn btn-success btn_editar_manual1" data-toggle="collapse" href="#form3"
                role="button" aria-expanded="false" aria-controls="ctn_evaluacion1" style="width: 100%;">DSM V</a>
                </div>
            </div>
        </div>



        <!-- Manual 1-->
        
        <div class="collapse multi-collapse" id="form1">
            NUEVO 1
            <form action="{{route('guardarManualBD', 50)}}" method="post" role="form">
                @method('POST')
                @csrf
                <div class="evaluacion__1">
                    <label for="">Trastornos clinicos</label>
                    <textarea class="form-control" id="txt_evaluacion_eje1" name="txt_evaluacion_eje1" rows="2"
                        placeholder="Trastornos Clínicos" required></textarea>
                </div>
                <div class="evaluacion__1">
                    <label for="">Transtornos mentales</label>
                    <textarea class="form-control" id="txt_evaluacion_eje2" name="txt_evaluacion_eje2" rows="2"
                        placeholder="Trastornos de la persona" required></textarea>
                </div>
                <div class="evaluacion__1">
                    <label for="">Enfermedades medicas</label>
                    <textarea class="form-control" id="txt_evaluacion_eje3" name="txt_evaluacion_eje3" rows="2"
                        placeholder="Enfermedades Médicas" required></textarea>
                </div>
                <div class="evaluacion__1">
                    <label for="">Problemas psicosociales y ambientales</label>
                    <textarea class="form-control" id="txt_evaluacion_eje4" name="txt_evaluacion_eje4" rows="2"
                        placeholder="EAG (0-100)" required></textarea>
                </div>
                <div class="evaluacion__1">
                    <label for="">EEAG</label>
                    <textarea class="form-control" id="txt_evaluacion_eje5" name="txt_evaluacion_eje5" rows="2"
                        placeholder="EEAG (0-100)" required></textarea>
                </div>
                <div class="evaluacion__1">
                    <input type="hidden" name="tipo_oculto" value="2">
                    <button type="submit" class="btn btn-primary" id="manual2" name="btn_manual2">Guardar
                        Cambios</button>
                </div>
            </form>
        </div>
        
        

        <!-- Manual 2-->
        <div class="collapse multi-collapse" id="form2">
            NUEVO 2
            <form action="{{route('guardarManualBD', 50)}}" method="post" role="form">
                @method('POST')
                @csrf
                <div class="evaluacion__1">
                    <label for="">Sindromes psiquiatricos</label>
                    <textarea class="form-control" id="txt_evaluacion_eje1" name="txt_evaluacion_eje1" rows="2"
                        placeholder="Evaluación global de la discapacidad" required></textarea>
                </div>
                <div class="evaluacion__1">
                    <label for="">Trastornos de desarrollo</label>
                    <textarea class="form-control" id="txt_evaluacion_eje2" name="txt_evaluacion_eje2" rows="2"
                        placeholder="Afectación del funcionamiento (WHODAS 2.0)" required></textarea>
                </div>
                <div class="evaluacion__1">
                    <label for="">Nivel intelectual</label>
                    <textarea class="form-control" id="txt_evaluacion_eje3" name="txt_evaluacion_eje3" rows="2"
                        placeholder="Otras condiciones que puedan ser objeto de atención clínica" required></textarea>
                </div>
                <div class="evaluacion__1">
                    <label for="">Condiciones medicas</label>
                    <textarea class="form-control" id="txt_evaluacion_eje4" name="txt_evaluacion_eje4" rows="2"
                        placeholder="Condiciones Médicas" required></textarea>
                </div>
                <div class="evaluacion__1">
                    <label for="">Situaciones psicosociales</label>
                    <textarea class="form-control" id="txt_evaluacion_eje5" name="txt_evaluacion_eje5" rows="2"
                        placeholder="Situaciones Psicosociales" required></textarea>
                </div>
                <div class="evaluacion__1">
                    <label for="">Evaluación de discapacidad</label>
                    <textarea class="form-control" id="txt_evaluacion_eje6" name="txt_evaluacion_eje6" rows="2"
                        placeholder="Evaluación Global de la Discapacidad" required></textarea>
                </div>
                <div class="evaluacion__1">
                    <input type="hidden" name="tipo_oculto" value="2">
                    <button type="submit" class="btn btn-primary" id="manual2" name="btn_manual2">Guardar
                        Cambios</button>
                </div>
            </form>
        </div>


        <!-- Manual 3-->
        <div class="collapse multi-collapse" id="form3">
            <form action="{{route('guardarManualBD', 50)}}" method="post" role="form" id="form3">
                MANUAL 3
                @method('POST')
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="evaluacion__1">
                            <label for="">Eje 1</label>
                            <textarea class="form-control" id="txt_evaluacion_eje1" name="txt_evaluacion_eje1" rows="2"
                                placeholder="Evaluación global de la discapacidad" required></textarea>
                        </div>
                        <div class="evaluacion__1">
                            <label for="">Eje 2</label>
                            <textarea class="form-control" id="txt_evaluacion_eje2" name="txt_evaluacion_eje2" rows="2"
                                placeholder="Afectación del funcionamiento (WHODAS 2.0)" required></textarea>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="evaluacion__1">
                            <label for="">Eje 3</label>
                            <textarea class="form-control" id="txt_evaluacion_eje3" name="txt_evaluacion_eje3" rows="2"
                                placeholder="Otras condiciones que puedan ser objeto de atención clínica"
                                required></textarea>
                        </div>
                    </div>
                </div>
                <div class="evaluacion__1 d-flex p-2">
                    <input type="hidden" name="tipo_oculto" value="3">
                    <button type="submit" class="btn btn-primary" id="manual3" name="btn_manual3">Guardar
                        Cambios</button>
                </div>
            </form>
        </div>

        @if(session()->has('successm3'))
            <div class="alert alert-success" role="alert">{{session('successm3')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="{{asset('assets/js/ficha/forms.js')}}"></script>

@endsection