@extends('layouts.dashboard')
@section('contentSidebar')
<!-- INICIO BUSCADOR-->
<div class="" id="buscador">
    <nav class="navbar navbar-light bg-light">
        <form method="get">
            <div class="form">
                <div class="form-row">
                    <div class="col-6">
                        <h2>Búsqueda de Pacientes</h2>
                        <label for="">Estado de Paciente</label>
                        <select class="custom-select mr-sm-2" id="usuarios" name="estado">
                            <option value="">Todos</option>
                            <option value="1">Activo</option>
                            <option value="2">Historico</option>
                        </select>
                    </div>

                </div>
                <br>
                <div class="form-row">
                    <div class="col-3">
                        <input name="name" class="form-control" type="search" placeholder="Nombre paciente">
                    </div>
                    <div class="col-3">
                        <input name="rut" class="form-control" type="search" placeholder="Rut paciente">
                    </div>
                    <div class="col-3">
                        <input name="id" class="form-control" type="search" placeholder="N° Ficha paciente">
                    </div>
                    <div class="col-3">
                        <button class="btn btn-info" type="submit">Buscar paciente</button>
                    </div>
                </div>

            </div>
        </form>
    </nav>
</div>
<!-- TÉRMINO BUSCADOR-->

<!-- INICIO TABLA RESULTADOS -->
<div id="tablalistar">
    <table class="table table-light">
        <thead class="thead-light">
            <tr>
                <th>Rut</th>
                <th>Nombre</th>
                <th>Estado</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>
        
        

        
            @foreach ($pacientes as $paciente)
            
            @if($paciente->id_profesional==$profesional)
                <th>{{$paciente->rut}}</th>
                <th>{{$paciente->name}} {{$paciente->apellido}}</th>


                <th>

                    @if ($paciente->estado === 1)
                    Activo
                    @else
                    Histórico
                    @endif

                </th>

                <th><a class="btn btn-info btn-sm" href="{{route('information', $paciente->id)}}">Ver Ficha</a></th>

            </tr>
            @endif
            @endforeach
        
        </tbody>
    </table>
    {!!$pacientes->render()!!}
    <!-- TÉRMINO TABLA RESULTADOS -->
</div>

</div>
<!-- TÉRMINO SECCIÓN DERECHA -->
@endsection