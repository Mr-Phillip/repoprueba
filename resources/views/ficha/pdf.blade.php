<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/pdf_style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <title>Ficha Médica Paciente</title>
</head>

<body>
    <div class="text-center encabezado_pdf">
        <!-- <img src="https://drive.google.com/file/d/1PJevYF41L0dyPphmVCiygs94D-gZ1ygJ/view?usp=sharing" alt="Logo Psicólogos Temuco"> -->
        <h1>Diagnóstico Paciente</h1>
        <h5>Psicólogos Temuco</h5>
    </div>
    <div class="contenedor_sesiones">
        <table class="table table-bordered letra_tabla table_paciente">
            <thead>
                <tr>
                    <th scope="col">N° de Ficha</th>
                    <th scope="col">Run</th>
                    <th scope="col">Nombre</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th><label for="">{{$paciente->id}}</label></th>
                    <td><label for="">{{$paciente->rut}}</label></td>
                    <td><label for="">{{$paciente->name }} {{ $paciente->apellido}}</label></td>
                </tr>
            </tbody>
        </table>
        <div class="item_pdf">
            <h4 class="text-left subtitulo">Diagnóstico</h4>
            <table class="table table-bordered letra_tabla table_info">
                <tbody>
                    <tr>
                        <th scope="row">Fecha Creación :</th>
                        <td colspan="2">{{$diagnostico->created_at}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Fecha Última Actualización :</th>
                        <td colspan="2">{{$diagnostico->updated_at}}</td>
                    </tr>
                    <tr>
                        <th scope="row">Descripción :</th>
                        <td colspan="2">{{$diagnostico->descripcion}}</td>
                    </tr>
                </tbody>
            </table>
        </div>

    </div>

</body>
</body>

</html>