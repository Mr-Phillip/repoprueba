@extends('layouts.dashboard')
@section('contentSidebar')
<!-- INICIO MENÚ 'VER DIAGNÓSTICO PSICOLÓGICO'-->
<div class="container_information">
    <div class="row_diagnostico_manual">
        <div class="col">
            <a href="{{route('information', $paciente->id)}}" class="btn btn-danger">Volver</a>
        </div>
        <div class="col">
            <h3 class="card-title">Menú Ver Diagnósticos</h3>
        </div>
        <div class="col">
            <h5>Paciente : {{$paciente->name}} {{$paciente->apellido}}</h5>
            <h5>N° Ficha : {{$paciente->id}}</h5>
        </div>
    </div>
</div>
<!-- TÉRMINO MENÚ 'VER DIAGNÓSTICO PSICOLÓGICO' -->
<!-- INICIO SECCIÓN MOSTRAR DIAGNÓSTICOS -->
<div class="card text-center container_information">
    <div class="card-body">
        <h3 class="card-title">Diagnósticos realizados</h3>
        <div class="evaluacion">
            <!-- Permite la edición de diagnóstico si existe en base de datos -->
            @if(@isset($diagnostico))
            <div class="form-row">

                <div class="col">
                    <label for="">Fecha Creación {{$diagnostico->created_at}}</label>
                </div>
                <div class="col">
                    <label for="">Última actualización {{$diagnostico->updated_at}}</label>
                </div>
            </div>
            <!-- <textarea class="form-control" readonly id="txt_descripcion_diagnostico" name="txt_descripcion_diagnostico" rows="4"></textarea> -->
            <p>{{$diagnostico->descripcion}}</p>
            <div class="col">
                <!-- <a href="#" class="btn btn-info" id="editar_diagnostico" name="editar_diagnostico">Editar Diagnóstico</a> -->
                <a href="{{ route('downloadPdf', $diagnostico->id)}}" class="btn btn-danger" id="descargar_pdf" name="descargar_pdf">Descargar PDF</a>
                <a class="btn btn-success btn_editar_diagnostico" data-toggle="collapse" href="#ctn_editar_diagnostico" role="button" aria-expanded="false" aria-controls="ctn_evaluacion1">Editar Diagnóstico</a>
                <div class="collapse multi-collapse ctn_comentario" id="ctn_editar_diagnostico">
                    <form action="{{route('updateDiagnostico', $diagnostico->id)}}" method="post" role="form">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <textarea class="form-control" id="txt_editar_diagnostico" name="txt_editar_diagnostico" rows="8" required>{{$diagnostico->descripcion}}</textarea>
                        </div>
                        <div class="evaluacion__1">
                            <button type="submit" class="btn btn-primary" id="manual3" name="btn_manual3" onclick="return confirmation()">Guardar Cambios</button>
                            <!-- Alerta de confirmación -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @else
        <!-- Si no existe diagnóstico en base de datos -->
        <div class="card text-center container_information">
            <div class="card-body">
                <h3 class="card-title">No existe diagnóstico</h3>
                <a href="{{route('create', $paciente->id)}}" class="btn btn-primary">Crear Diagnóstico</a>
            </div>
        </div>
        @endif
        <!-- Alert -->
        @if(session()->has('success'))
        <div class="alert alert-success" role="alert">{{session('success')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <!-- Alert -->
    </div>

    <!-- TÉRMINO SECCIÓN MOSTRAR DIAGNÓSTICOS -->
    <!-- INICIO MOSTRAR MANUALES -->
    @include('ficha.manuales')

    <!-- TÉRMINO MOSTRAR MANUALES -->
    <!-- INICIO SECCIÓN COMENTARIOS -->
        <!-- Sección comentarios -->
    <div class="table-responsive">   
    <table class="table table-bordered ">
        <thead class="thead-light">
            <tr>
                <th>Observaciones</th>
                <th>Fecha Creación</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($observaciones as $observacion)
            <tr>
                <div class="col-9">
                    <td class="text-left">{{$observacion->observacion}}</td>
                </div>
                <div class="col-3">
                    <td>{{$observacion->created_at}}</td>
                </div>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
        <!-- Fin sección comentarios -->


    <a class="btn btn-primary btn_comentario" data-toggle="collapse" href="#ctn_comentario" role="button" aria-expanded="false" aria-controls="ctn_evaluacion1">Crear nuevo comentario</a>
    <div class="collapse multi-collapse ctn_comentario" id="ctn_comentario">
        <form action="{{route('guardarComentarioBD', $paciente->id)}}" method="post" role="form">
            @csrf
            <input type="hidden" name="idDiag" value="{{ $diagnostico->id }}">
            <div class="form-group">
                <textarea class="form-control" id="txt_nuevo_comentario" name="txt_nuevo_comentario" rows="2" required></textarea>
            </div>
            <div class="evaluacion__1">
                <button type="submit" class="btn btn-primary" id="comentario" name="btn_comentario" onclick="return confirmation()">Guardar Comentario</button> </div>
    </div>
    </form>
    <!-- Alert -->
    @if(session()->has('successgc'))
    <div class="alert alert-success" role="alert">{{session('successgc')}}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <!-- Alert -->

</div>
<!-- TÉRMINO SECCIÓN COMENTARIOS -->

@endsection