@extends('pasareladepago.webpay.layoutvoucher')

@section('script')
    <script type='text/javascript'>
    window.onload = updateClock;
        var totalTime = 15;
            function updateClock() {
                document.getElementById('relojito').innerHTML = totalTime;

                if(totalTime==0){
                        if ( {{Session::get('pago')}}) {
                            window.location.replace("/pasareladepago/webpay/ordencompra/{{ Session::get('pago')}}")
                        } else {
                            window.location.replace('/pasareladepago/webpay/listCitas');
                        }
                } else {
                        totalTime-=1;
                        setTimeout('updateClock()', 1500);
                    }
                }

</script>
@endsection

@if (Session::get('responseCode') !== null)
    @switch(Session::get('responseCode'))
        @case(0)
            @php
                Session::forget('responseCode');
            @endphp
            @section('msje')
                    <div class='alert alert-success text-center'>
                        ¡Se ha realizado con éxito su reserva y su cita se encuentra pagada!
                        Se ha enviado una copia a su correo registrado.
                        Este mensaje es automático, será redirigido al detalle de su pago en
                        <span id='relojito'>15</span> segundos...
                    </div>
                    <object id="pdf" class="embed-responsive-item"
                        data="{{route('pasareladepago.webpay.visualizaciondetalle', Session::get('pago'))}}#zoom=0&toolbar=1&navpanes=0&scrollbar=0"
                        type="application/pdf" width="100%" height="600px">
                            <embed class="embed-responsive-item"
                            src="{{route('pasareladepago.webpay.visualizaciondetalle', Session::get('pago'))}}#zoom=0&toolbar=1&navpanes=0&scrollbar=0"
                            type="application/pdf" sandbox/>
                    </object>
            @endsection
            @php
                Session::forget('pago');
            @endphp
            @break
        @case(-1)
            @php
                Session::forget('responseCode');
                Session::forget('pago');
            @endphp
            @section('msje')
                <div class='alert alert-danger text-center'>
                    Ha ocurrido un error al procesar la transacción. Por favor, verifique su cuenta.
                    Este mensaje es automático, será redirigido en <span id='relojito'>15</span> segundos...
                </div>
            @endsection
            @break
        @case(-2)
                @php
                    Session::forget('responseCode');
                    Session::forget('pago');
                @endphp
                    @section('msje')
                            <div class='alert alert-danger text-center'>
                                Ha ocurrido un error al procesar la transacción. Por favor, verifique su cuenta.
                                Este mensaje es automático, será redirigido en <span id='relojito'>15</span> segundos...
                        </div>
                    @endsection
            @break
        @case(-3)
            @php
                Session::forget('responseCode');
                Session::forget('pago');
            @endphp
            @section('msje')
                <div class='alert alert-success text-center'>
                    Ha ocurrido un error interno desde Transbank. Por favor, verifique su cuenta.
                    Este mensaje es automático, será redirigido en <span id='relojito'>15</span> segundos...
                </div>
            @endsection
            @break
        @case(-4)
            @php
                Session::forget('responseCode');
                Session::forget('pago');
            @endphp
            @section('msje')
                <div class='alert alert-success text-center'>
                    Ha ocurrido un error, la transacción ha sido rechazada. Favor consulte con el banco de su tarjeta.
                    Este mensaje es automático, será redirigido en <span id='relojito'>15</span> segundos...
                </div>
            @endsection
            @break
        @case(-5)
            @php
                Session::forget('responseCode');
                Session::forget('pago');
            @endphp
            @section('msje')
                    <div class='alert alert-danger text-center'>
                        ¡Ha ocurrido un error, verifique su estado de su cuenta con su banco!
                        Este mensaje es automático, será redirigido en <span id='relojito'>15</span> segundos...
                    </div>
            @endsection
            @break
    @endswitch
@else
            @php
                Session::forget('responseCode');
                Session::forget('pago');
            @endphp
            @section('msje')
                <div class='alert alert-danger text-center'>
                    El pago ha sido anulado, no se ha generado ningun cargo a su tarjeta.
                    Este mensaje es automático, será redirigido en <span id='relojito'>15</span> segundos...
                </div>
            @endsection
@endif

{{-- <script type='text/javascript'>
    window.onload = updateClock;
        var totalTime = 10;
            function updateClock() {
                document.getElementById('relojito').innerHTML = totalTime;
                if(totalTime==0){
                        window.location.replace('/pasareladepago/webpay/listCitas');
                        window.open('/pasareladepago/webpay/visualizardetalle/18', '_blank');

                } else {
                        totalTime-=1;
                        setTimeout('updateClock()',1000);
                    }
                }
</script> --}}

