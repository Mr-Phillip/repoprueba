@extends('layouts.dashboard')
@extends('layouts.app')
@section('contentSidebar')

@if (session('mensaje'))
<div class="alert alert-success text-center">
    {{session('mensaje')}}
</div>
@endif

<div class="container">
    <div class="table-responsive">
        <div class="text-center mb-4 mt-5">
            <h1 class="title-1">Lista de Citas</h1>

        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col" class="title-4 text-center">N°</th>
                    <th scope="col" class="title-4 text-center">Fecha</th>
                    <th scope="col" class="title-4 text-center">Hora inicio</th>
                    <th scope="col" class="title-4 text-center">Hora Termino</th>
                    <th scope="col" class="title-4 text-center">Modalidad</th>
                    <th scope="col" class="title-4 text-center">Confirmación</th>
                    <th scope="col" class="title-4 text-center">Pago</th>
                    <th scope="col" class="title-4 text-center">Prevision</th>
                    <th scope="col" class="title-4 text-center">Isapre</th>
                    <th scope="col" class="title-4 text-center">Precio</th>
                    <th scope="col" class="title-4 text-center">Pagar</th>
                </tr>
            </thead>
            <div>
                <form action="{{route('pasareladepago.webpay.filtroEstado')}}" method="GET">
                    @csrf
                    <div class="input-group mb-3">
                        <select class="custom-select" name="estadoPago">
                            <option value='0' selected>Seleccionar...</option>
                            <option value="Pendiente">Pendiente</option>
                            <option value="Pagado">Pagado</option>
                        </select>
                        <div class="input-group-append">
                            <input class="btn btn-primary" style="background-color: #484AF0;" type="submit" value="Filtrar">
                        </div>
                    </div>
                </form>
            </div>
            <br>
            <tbody>
                @foreach ($cita as $citas)

                <tr>
                    <th scope="row" class="text-center">{{$rank++}}</th>
                    <td class="text-center">{{$citas->fecha}}</td>
                    <td class="text-center">{{$citas->hora_inicio}}</td>
                    <td class="text-center">{{$citas->hora_termino}}</td>
                    <td class="text-center">{{$citas->modalidad}}</td>
                    <td class="text-center" style="color: #FFF">
                        @if ($citas->estado === 'Sin Confirmar')
                        <span class="badge bg-danger">{{$citas->estado}}</span>
                        @elseif ($citas->estado === 'Confirmado')
                        <span class="badge bg-warning">{{$citas->estado}}</span>
                        @else
                        <span class="badge bg-success">{{$citas->estado}}</span>
                        @endif
                    </td>
                    <td class="text-center" style="color: #FFF">
                        @if($citas->estado_pago === 'Pagado')
                        <span class="badge bg-success">{{$citas->estado_pago}}</span>
                        @else
                        <span class="badge bg-danger">{{$citas->estado_pago}}</span>
                        @endif
                    </td>
                    <td class="text-center">{{$citas->prevision}}</td>
                    <td class="text-center">{{$citas->isapre}}</td>
                    <td class="text-center">{{$citas->precio}}</td>
                    @if($citas->estado_pago=="Pendiente")
                    <td class="text-center" style="color: #FFF">
                        <form action="{{route('pasareladepago.webpay.pagar')}}" method="POST">
                            @csrf
                            <input type="hidden" name="citaID" value="{{$citas->id}}">
                            <button type="submit" class="btn btn-danger">Pagar</button>
                        </form>
                    </td>
                    @else
                    <td>
                        <button type="button" class="btn btn-block btn-success" disabled>
                            <i class="far fa-check-circle" style="font-size: 24px;"></i>
                        </button>
                    </td>
                    @endif
                </tr>

                @endforeach
            </tbody>
        </table>
        @if(Session::has('aviso'))
        <div>{{Session::get('aviso')}}</div>
        @endif
    </div>
</div>
<ul class="pagination justify-content-center">{{$cita->appends(request()->query())->links()}}</ul>
@stop
