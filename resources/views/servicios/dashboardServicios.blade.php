<!-- sección de scripts -->
    <!-- Scripts para mostrar/esconder div del formulario y botones de eliminar -->

    <script src="{{ asset('assets/js/servicios/hideShowDiv.js') }}"></script>

    <!-- Script para validar el formulario de los servicios -->

    <script src="{{ asset('assets/js/servicios/validarFormServices.js') }}"></script>

    <!-- Script para validar el formulario de los servicios al editar -->

    <script src="{{ asset('assets/js/servicios/validarFormEditar.js') }}"></script>

@extends('layouts.dashboard')
@section('contentSidebar')
    <div class="container-fluid" id="list-servicio"></div>
@endsection

<input type="hidden" id="idPerfil" value="{{auth()->id()}}">

<!-- Script para traer los servicios y mostrarlos en el div -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>

    $(document).ready(function(){
        listServicio();
    });

    var listServicio = function(){
        var idPerfil = document.getElementById('idPerfil').value;
        var dataString = "id="+idPerfil;
        $.ajax({
            type: 'get',
            url: '{{url('serviciosall')}}',
            data: dataString,
            success: function(data){
                $('#list-servicio').empty().html(data);
            }
        });
    }

</script>