<div class="banner w-100 mt-5">
    <div class="text-center mb-5">
        <h1 class="title-1 mb-5 darkblue-text">¿Necesitas ayuda? </h1>
        <h1 class="text-1 mb-5 bluegray-text text-regular">
            Bienvenido a Psicólogos Temuco somos la red de terapia online nacional mas grande.
        </h1>
        <div class="row d-flex justify-content-center">
           <!-- <div class="col-lg-5 mb-3">
                <a href="{{url('/foro')}}" class="btn indigo white-text text-4 text-medium btn-block p-2">
                    Ir al foro <i class="far fa-comments fa-fw pink-text"></i> 
                </a>  
            </div>-->
            <div class="col-lg-5">
                 <a href="" class="btn transparent indigo-text text-4 text-medium btn-block p-2">
                    Conoce el proyecto <i class="fas fa-arrow-right fa-fw pink-text"></i> 
                </a>
            </div>
            
        </div> 

    </div>
</div>

