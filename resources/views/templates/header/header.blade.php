<div class="header w-100">
    <div class="d-flex justify-content-center">
        <div class="container p-lg-0 pl-4 pr-4  inner">
            <div class="row mt-5 w-100 d-flex justify-content-center">
                <div class="col-lg-6 p-0">
                    @include('templates.header.banner')
                </div>
            </div>
        </div>
    </div>
</div>